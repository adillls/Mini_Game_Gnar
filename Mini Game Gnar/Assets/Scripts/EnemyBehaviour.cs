﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
	

	public Transform enemyHome;
	private Transform player;
	private Vector2 positionPlayerLost;
	private Vector2 positionPlayerFind;
	private Transform enemy;
	public float startTime;
	public bool lostPlayer = true;

	public Animator enemyAnimator;
	public float speed;
	public float speedChase;
	private Rigidbody2D rb;
	private bool lookToRight = true;
	private Vector2 facingRight;
	private Vector2 facingLeft;
	public Transform footCollision; //onde checar se player está no chão
	public Transform wallCollision; //onde checar se player está colidindo com paredes
	public Transform enemyCollision;
	private bool isOnWall=false;
	private bool isOnEnemy=false;
	private bool isGrounded=false; 
	public float rayWall;
	public float rayGround;
	private Vector3 sideBeforeChase;
	private float dist=1f;
    public int points = 1;
	 //buscar pela tag do player
    float nextTimeToSearch = 0;
    private bool searchingForPlayer = false;
    private bool died = false;

    // antes do start
    void Awake() {
		enemy = GetComponent<Transform>();
        //if(player != null)
        //player = GameObject.FindWithTag ("Player").transform;     
        rb = GetComponent<Rigidbody2D> ();

        if (player == null) // inicia os IEnumerators/Corrotines que buscam o player.
        {
            if (!searchingForPlayer) {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }

		facingRight = transform.localScale;
		facingLeft = facingRight;
		facingLeft.x *= -1;
	}

    IEnumerator SearchForPlayer() {
       GameObject sResult= GameObject.FindGameObjectWithTag("Player");
        if (sResult == null) {
            player = enemy; 
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        } else{
            player = sResult.transform;
            searchingForPlayer = false;
            StartCoroutine (UpdatePath());

           yield  return false;
        }
    }
    IEnumerator UpdatePath()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            yield return false;
        }
    }

    void Update () {

        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());//"para" a exec até terminar 
            }
            return;
        }

		//raycast de perseguição
        Ray2DChase ();

			if (lostPlayer) {
				Move();
				enemy.localScale = sideBeforeChase;
			}		 
			else if(!lostPlayer)//persegue até perder
			{
				//faz a movimentação suave de um um ponto a outro com atraso x
				//enemy.position = Vector2.Lerp (enemy.position, player.position, 0.03f);
				transform.position = Vector3.MoveTowards(enemy.position, new Vector3(player.position.x, enemy.position.y), speedChase* Time.deltaTime );
                //transform.Translate(-enemy.position *(speedChase * Time.deltaTime));
                LookPlayer();
			}


		
	}

	public void Ray2DChase()
	{
		RaycastHit2D hit = Physics2D.Raycast (transform.position,(enemy.right* enemy.localScale.x), dist, 1 << LayerMask.NameToLayer("Player"));
		if (hit) {
			lostPlayer = false;
			//Debug.Log (hit.collider.name);
		} else {
			lostPlayer = true;
		}
	}
	public void Move()	{

		isOnWall = Physics2D.Raycast (transform.position,(enemy.right* enemy.localScale.x), 0.2f, 1 << LayerMask.NameToLayer("Ground"));
		isOnEnemy = Physics2D.OverlapCircle (enemyCollision.position, rayWall, 1 << LayerMask.NameToLayer("Enemy"));
		isGrounded = Physics2D.OverlapCircle (footCollision.position, rayGround, 1 << LayerMask.NameToLayer("Ground"));

		if ((isOnWall  || !isGrounded || isOnEnemy) && lookToRight) {			
			FlipWalk ();
		} else if ((isOnWall || !isGrounded || isOnEnemy) && !lookToRight) {			
			FlipWalk ();
		}
		if (isGrounded) {
			rb.velocity = new Vector2 (speed, rb.velocity.y);
		}
	}

	public void LookPlayer()
	{
		if(player.position.x > transform.position.x){
			//face right
			transform.localScale = new Vector3(1,1,1);
		}else if(player.position.x < transform.position.x){
			//face left
			transform.localScale = new Vector3(-1,1,1);
		}
	}

	//inverte sprite e direção qual o inimigo anda
	void FlipWalk(){
			lookToRight = !lookToRight;
			transform.localScale = new Vector2 (-transform.localScale.x, transform.localScale.y);

			speed *= -1;
			sideBeforeChase = enemy.localScale; //salva último lado qual inim olhava antes de perseguir
	}

	public void Animations(){
		//atualiza a variavel da animação com o valor do Linecast
		enemyAnimator.SetBool("isGrounded", isGrounded);

		//define velocidade de queda. fins de animação
		enemyAnimator.SetFloat ("vSpeed", rb.velocity.y);
	}

	void OnDrawGizmosSelected(){
		//função para debug - mostra raio de colisão na cena
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (wallCollision.position, rayWall);
		Gizmos.DrawWireSphere (footCollision.position, rayGround);
		//Gizmos.DrawWireSphere (wallCollision2.position, rayWall);
	}

	public void Die()
	{
        if (!died)
        {
            died = true;
            GameMaster.KillEnemy(this);
        }
    }

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
		
			lostPlayer = false;
		}
	}
}