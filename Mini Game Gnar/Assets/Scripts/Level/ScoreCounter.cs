﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreCounter : MonoBehaviour
{      
    private Text ScoreText;

    // Use this for initialization
    void Awake()
    {       
        ScoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {        
        ScoreText.text = "Score: " + GameMaster.Points.ToString();

    }
}
