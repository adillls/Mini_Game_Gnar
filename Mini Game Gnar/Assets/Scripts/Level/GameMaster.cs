﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour{

    private static GameMaster gm;
	//classe responsavel por regras

    [SerializeField]
    private int maxLives = 3;
    private static int _remainingLives;
    public static int RemainingLives
    {
        get { return _remainingLives; }
    }

    [SerializeField]
    private int startingpontuation = 0;
    private static int pontuation;
    public static int Points
    {
        get { return pontuation; }
    }

    public void Awake()
    {
        if (gm == null)
        {
            gm = this;
           //DontDestroyOnLoad(gameObject);
            //Debug.Log("OBJ = " + gm);
        }
        else {
            DestroyImmediate(gameObject);
        }

        _remainingLives = maxLives;
        pontuation = 0;
        pontuation = startingpontuation;
    }

    public void Start() {
       /* _remainingLives = maxLives;
        pontuation = 0;
        pontuation = startingpontuation;
        */
    }

	public Transform playerPrefab;
    public Transform spawnPoint;
    public float spawnDelay = 2f;    
    
    [SerializeField]
    private GameObject gameOverUI; //tela de gameover

    public static void End() {
        gm.EndGame();
    }
    public void EndGame()
    {
        //audioManager.PlaySound(gameOverSoundName);       
        Debug.Log("GAME OVER");
        gameOverUI.SetActive(true);
    }

    public IEnumerator _RespawnPlayer()
    {
        yield return new WaitForSeconds(spawnDelay);
		Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}  

    public static void KillPlayer(PlayerBehavior player)
    {
        Destroy(player.gameObject);
        _remainingLives -= 1;
        if (_remainingLives <= 0)
        {
            gm.EndGame();
        }
        else
        {
            gm.StartCoroutine (gm._RespawnPlayer());
    	}
    }

    public static void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        Debug.Log("Próxima fase!");
    }

    public static void KillEnemy(EnemyBehaviour enemy)
    {
        gm._KillEnemy(enemy);       
    }
    public void _KillEnemy(EnemyBehaviour _enemy)
    {

        pontuation += _enemy.points;
        Debug.Log(pontuation);
        Destroy(_enemy.gameObject);
    }
}