﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUI : MonoBehaviour {

    [SerializeField]
    WaveSpawn spawn;

    [SerializeField]
    Animator waveAnimator;

    [SerializeField]
    Text levelText;

    [SerializeField]
    Text numberText;
    private WaveSpawn.SpawnState previousState;

    // Use this for initialization
    void Start () {
        if (spawn == null)
        {
            Debug.Log("No spawn referenced!");
        }
       
            if (spawn == null)
            {
                Debug.Log("No spawn referenced!");
            this.enabled = false;
            }
            if (waveAnimator == null)
            {
                Debug.Log("No Animator referenced!");
            this.enabled = false;
             }
            if (levelText == null)
            {
                Debug.Log("No Level referenced!");
            this.enabled = false;
            }
            if (numberText == null)
            {
                Debug.Log("No number referenced!");
            this.enabled = false;
             }
        }

	// Update is called once per frame
	void Update () {
        switch (spawn.State)
        {
            case WaveSpawn.SpawnState.COUNTING:
                UpdateLevelStartUI();
                break;
            case WaveSpawn.SpawnState.SPAWNING:
                UpdateIncomingUI();
                break;            
        }
        previousState = spawn.State;
	}

    void UpdateLevelStartUI()
    {  
        if (previousState != WaveSpawn.SpawnState.COUNTING)
        {
            waveAnimator.SetBool("Number", false);
            waveAnimator.SetBool("Level", true);
            //Debug.Log("COUNTING");
	    }
    }
    void UpdateIncomingUI()
    {
        if (previousState != WaveSpawn.SpawnState.SPAWNING)
        {
            waveAnimator.SetBool("Number", true);
            waveAnimator.SetBool("Level", false);
            //Debug.Log("SPAWNING");
        }
        numberText.text = ((int)spawn.NextWave).ToString();
    }
    void UpdateevelStartUI()
    {
        Debug.Log("COUNTING");
    }
}