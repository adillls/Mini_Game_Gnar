﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuMaster : MonoBehaviour {

    [SerializeField]
    Scene Level1;

	[SerializeField]
    private GameObject credit;
	[SerializeField]
	private GameObject control;
    public void StartGame() {

        SceneManager.LoadScene(1);

    }

    public void Quit()
    {
        
        Application.Quit();

    }

    public void Menu()
    {

        SceneManager.LoadScene(0);

    }

    public void Credit()
    {
        Debug.Log("Creditos!");
        credit.SetActive(true);

    }
	public void  Control()
	{
		Debug.Log("Controle");
		control.SetActive(true);

	}

}
