﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRadar : MonoBehaviour {
	//substituido por raycast

	private EnemyBehaviour script;

	// Use this for initialization
	void Start () {

		script = GetComponentInParent<EnemyBehaviour>();
		
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
			script.lostPlayer = false;
			//script.canChase = true;

				
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "Player") {
			//script.BackToHome (); //colocar aki o script de move
			script.lostPlayer = true;
			//script.canChase = false;

		}
	}

}
