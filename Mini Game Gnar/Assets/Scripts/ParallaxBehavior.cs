﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBehavior : MonoBehaviour {

	public Transform target;	
	public float maxLimit;
	public float minLimit;
	public float maxLimitY;
	public float minLimitY;
	public List<ItemParallax> itensParallax;
    float nextTimeToSearch = 0;


	[System.Serializable]//Serialization is the automatic process of transforming data 
	//structures or object states into a format that Unity can store and reconstruct later
	public class ItemParallax
	{
		public Transform item;
		public float maxLimit;
		public float minLimit;
		public float moveFactor;

	}

	private Vector3 newPosition = new Vector3(0,0,-1);//x,y,z
	private Vector3 lastPosition;		

	// Use this for initialization
	void Start () {
		
	}
	
	// espera renderização ocorrer 
	void LateUpdate () {

        if(target == null)
        {
            FindPlayer();
            return;
        }
		if(target != null){
		newPosition.x = target.position.x; //recebe posição do player
		newPosition.y = target.position.y; //recebe posição do player
		}
		if (newPosition != lastPosition) { 
			//player se movimentou //last position começa com 00

			if (newPosition.x > maxLimit) {
			//verifica se atingiu o limite em x
				newPosition.x = maxLimit;
			}
			else if (newPosition.x < minLimit) {
				//verifica se atingiu o limite em x
				newPosition.x = minLimit;
			}

			if (newPosition.y > maxLimitY) {
				//verifica se atingiu o limite em x
				newPosition.y = maxLimitY;
			}
			else if (newPosition.y < minLimitY) {
				//verifica se atingiu o limite em x
				newPosition.y = minLimitY;
			}

			transform.position = newPosition;



			Vector3 newPositionItem;

			foreach (ItemParallax p in itensParallax){

				newPositionItem = p.item.localPosition; //posição relacionada ao envir

				if (newPositionItem.x > p.maxLimit) {
					//verifica se atingiu o limite em x
					newPositionItem.x = p.maxLimit;
				}
				else if (newPosition.x < p.minLimit) {
					//verifica se atingiu o limite em x
					newPositionItem.x = p.minLimit;
				}

				p.item.localPosition = newPositionItem; //recebe posição nova
				//verifica posição

				if(lastPosition.x < newPosition.x){
				//translate movimenta objeto
					p.item.Translate(Vector3.left*Time.deltaTime*p.moveFactor);
				}
				//verifica posição
				else if(lastPosition.x > newPosition.x){
					//translate movimenta objeto
					p.item.Translate(Vector3.right*Time.deltaTime*p.moveFactor);
				}




			}

			lastPosition = newPosition;
		}




	}


    void FindPlayer() {
        if (nextTimeToSearch <= Time.time) {
            GameObject searchResult = GameObject.FindGameObjectWithTag("Player");
            if(searchResult != null){
                target = searchResult.transform;
            }
            nextTimeToSearch = Time.time + 0.5f; //coloca um delay na procura da tag, fins de otimização
        }
    }

}
