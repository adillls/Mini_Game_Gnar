﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawn : MonoBehaviour {

	public enum SpawnState{ SPAWNING, WAITING, COUNTING};

	[System.Serializable]//allow to change values of instances of this class inside inspector
	public class Wave{
		public string name;
		public Transform enemy;
		public int count;
		public float rate;
		}

	public Wave[] waves;
	private int nextWave = 0;
	public int NextWave
	{
		get { return nextWave + 1;}
	}

	public Transform[] spawnPoints; 

	public float timeBetweenWaves = 5f; //tempo entre waves
	private float waveCountdown;
	public float WaveCountdown
	{
		get{ return waveCountdown;}
	}
	private float searchCountdown = 1f;

	private SpawnState state = SpawnState.COUNTING; 

	public SpawnState State 
	{
		get{ return state;}
	}

	void Start(){
		if (spawnPoints.Length == 0) {
			//Debug.LogError ("sem SpawPoints 1");
		}
		waveCountdown = timeBetweenWaves;
	}

	void Update(){
		if (state == SpawnState.WAITING) {
			if (EnemyIsAlive()== false) {
				//Debug.LogError (" 1 chamou o wave completed");
				WaveCompleted ();
				//Debug.LogError (" 1.5 voltou do wave completed");
			} else {
				return;

				//Debug.LogError (" 1.6 else do wave completed");
			}
		}

		if (waveCountdown <= 0) {
			if (state != SpawnState.SPAWNING) {
				StartCoroutine (SpawnWave (waves[nextWave]));
				//Debug.Log ("Iniciou coroutine 2");
			}
		}
		else {
			waveCountdown -= Time.deltaTime;
			//Debug.Log ("Iniciou coroutine o else tb 3");
		}
	}

	void WaveCompleted()
	{
		//Debug.Log ("WAVE TERMINADA 4");
		state = SpawnState.COUNTING;
		waveCountdown = timeBetweenWaves;
		if (nextWave + 1 > waves.Length - 1) {

            //nextWave = 0;
            //implementar aumento de dificuldade/ inimigo speed up
           GameMaster.NextLevel();

            Debug.Log ("Terminou todas waves! 5");

		} else {			
			nextWave++;
			Debug.Log ("Aki no else do nextWave 6" + nextWave);
		}
	}

	bool EnemyIsAlive()
	{
		//Debug.Log ("Entrou na func Alive 7");
		searchCountdown -= Time.deltaTime;
		if (searchCountdown <= 0f) {
			searchCountdown = 1f; 
			//Debug.Log(" 7 Não tem ninguem vivo " + GameObject.FindGameObjectWithTag("enemy"));
			if (GameObject.FindGameObjectWithTag("zzrot") == null)
			{
				
				return false;
				//Debug.Log ("8 o return false foi");
			}
		}
		return true;
	}

		IEnumerator SpawnWave (Wave _wave){
		
			//Debug.Log ("9 Tropa liberada:" + _wave.name);
			state = SpawnState.SPAWNING;
			for(int i=0; i< _wave.count; i++){
				SpawnEnemy (_wave.enemy); 
				yield return new WaitForSeconds (1f / _wave.rate);
			}
			state = SpawnState.WAITING;

			yield break;
		}

	void SpawnEnemy (Transform _enemy)
	{
		//Debug.Log("10 Saindo inimigo:"+ _enemy.name);

		Transform _sp = spawnPoints [Random.Range (0, spawnPoints.Length)];

		Instantiate (_enemy, _sp.position, _sp.rotation);//colocar vector 3
	}

}



