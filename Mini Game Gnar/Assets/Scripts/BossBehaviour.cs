﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossBehaviour : MonoBehaviour{


    public float life = 100;
    public Transform[] positionPoints;
    public Transform eye;
    public float speed;
    public Transform laser;
    public Transform enemy;
    private Transform player;
    private Vector3 playerPos;    
    private Vector2 facingRight;
    private Vector2 facingLeft;
    private Rigidbody2D rb;
    private Transform boss;    
    bool dead;
    float nextTimeToSearch = 0;
    private bool searchingForPlayer = false;
    private bool died = false;
	private bool enemys=true;
	private float searchCountdown = 1f;
    void Awake()
    {
        //test ia**


        rb = GetComponent<Rigidbody2D>();

        boss = GetComponent<Transform>();

       /* if (player == null) // inicia os IEnumerators/Corrotines que buscam o player.
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }
        Debug.Log("Iniciou o awake");
        */

    }
    void Start()
    {

        //if (player == null)
        player = GameObject.FindWithTag("Player").transform;

        facingLeft = transform.localScale;
        facingRight = facingLeft;
        facingRight.x *= -1;

        StartCoroutine("_boss");//inicia maquina de estados

    }


    IEnumerator SearchForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
           // player = boss; //test
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            player = sResult.transform;
            searchingForPlayer = false;
            StartCoroutine(UpdatePath());

            yield return false;
        }
    }
    IEnumerator UpdatePath()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            yield return false;
        }
     }

    // Update is called once per frame
    void Update()
    {
        /*
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());//"para" a exec até terminar 
            }
            return;
        }
        */

		if (life <= 0 && !dead)
		{
			dead = true;
			Debug.Log("Boss Eliminado!");
			GameMaster.End();        
			StopCoroutine("boss");           
		}

		if (EnemyIsAlive () == false) {			
			enemys = false;
			//Debug.LogError (" 1.5 voltou do wave completed");
		} else {
			enemys = true;
			return;
		}

    }

    IEnumerator _boss()
    {
        while (true)
        {
            ////////////////////////////////// FIRST ATTACK

            while (transform.position != positionPoints[0].position)//move até a posição
            {
                transform.position = Vector2.MoveTowards(transform.position, positionPoints[0].position, speed*Time.deltaTime);
				yield return null;
            }

            transform.localScale = facingRight; // flipa

            yield return new WaitForSeconds(1f);

            int i = 0;
            while (i < 8) //quantidade de laser
            {
               
                GameObject shot = Instantiate(laser.gameObject, eye.position, transform.rotation) as GameObject;
                //shot.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
                //shot.GetComponent<Rigidbody2D>().velocity = player.position * speed;
                shot.GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
                i++;
                yield return new WaitForSeconds(.8f); //tempo entre tiros
            }
			////////////////////////////////// SECOND ATTACK
            
			//GetComponent<Rigidbody2D>().isKinematic = true;
            while (transform.position != positionPoints[1].position)
            {
                transform.position = Vector2.MoveTowards(transform.position, positionPoints[1].position, speed * Time.deltaTime);

                yield return null;
            }

            //yield return new WaitForSeconds(1f);

             i = 0;
			while (i < 5) //spawna 5 inimigos
            {                
                Instantiate(enemy, transform.position, transform.rotation);
                i++;
                yield return new WaitForSeconds(.8f); //tempo entre tiros
            }
            //continua segundo ataque, vai para ponto 4
            while (transform.position != positionPoints[3].position)
            {
                transform.position = Vector2.MoveTowards(transform.position, positionPoints[3].position, speed * Time.deltaTime);

                yield return null;
            }

            transform.localScale = facingLeft;
            //yield return new WaitForSeconds(1f);

            i = 0;
			while (i < 5) //quantidade de spawn
            {
                //Instantiate(_enemy, _sp.position, _sp.rotation);
                Instantiate(enemy, transform.position, transform.rotation);
                i++;
                yield return new WaitForSeconds(.8f);
            }

			//test find enemy

			while (enemys == true)
			{
				Debug.Log("Tem inimigo vivo");
				yield return null;	

			}

				yield return new WaitForSeconds(.8f);
        }

    }

	void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "boomerang")
        {

            life -= 40;
			Debug.Log ("-40");
            //animar dano
        }
    }


	bool EnemyIsAlive()
	{		
		searchCountdown -= Time.deltaTime;
		if (searchCountdown <= 0f) {
			searchCountdown = 1f; 
			//Debug.Log(" 7 Não tem ninguem vivo " + GameObject.FindGameObjectWithTag("enemy"));
			if (GameObject.FindGameObjectWithTag("zzrot") == null)
			{
				return false;
			}
		}
		return true;
	}

}