﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour {

	public Animator playerAnimator;

	//moviment
	public float speed;
	public float maxSpeed;
	public float speedJump;
	public float maxSpeedJump;
	private Rigidbody2D rigidbodyPlayer;
	private bool lookToLeft = true;
	private Vector3 facingRight;
	private Vector3 facingLeft;
	private bool isGrounded=true; 
	public Transform footCollision; 
	GameObject boomer;
	public float rayWall;
	//boomerang
	public BoomerangBehaviour boomerang;
    private bool died=false; 


	// Use this for initialization
	void Start () {
		transform.tag = "Player"; //define automaticamente a tag
		rigidbodyPlayer = GetComponent<Rigidbody2D> ();
		facingLeft = transform.localScale;
		facingRight = facingLeft;
		//Recebe posição oposta original do sprite
		facingRight.x *= -1;
	}

	// Update is called once per frame
	void Update () {
		//passa entrada do teclado em x = -1 ou 1 //DeltaTime padroniza velocidade em qualquer maquina :
		Move(Input.GetAxis("Horizontal")*speed*Time.deltaTime);
		Attack ();
		//verifica tecla space executa pulo
		if (Input.GetKeyDown (KeyCode.Space ) || Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)){
			Jump();
		}
		Animations(); //chama todas as animações
	}

	public void Attack(){
		
		if(boomer == null){ 
			if (Input.GetKeyDown (KeyCode.Q) || Input.GetKeyDown (KeyCode.O)) { //&& !boomerang.returning Input.GetMouseButtonDown (0) || Input.GetMouseButtonDown (0) || 
				boomer =	Instantiate (boomerang.gameObject, transform.position, transform.rotation) as GameObject;
				boomer.GetComponent<BoomerangBehaviour> ().setDirection(!lookToLeft);
			}
		}	
	}
	private void Move(float horizontalInput)	{
		
		Vector2 inputDirection = new Vector2 (horizontalInput, 0);
		//altera lado da sprite
		if (inputDirection.x > 0) {
			lookToLeft = false;
		}
		if (inputDirection.x < 0) {
			lookToLeft = true;
		}
		if (lookToLeft) {
			transform.localScale = facingLeft; 
		} else {
			transform.localScale = facingRight; 
		}
			// substitui o rigidbodyPlayer.AddForce (inputDirection) tornando o andar mais suave
			rigidbodyPlayer.velocity = new Vector2 (inputDirection.x, rigidbodyPlayer.velocity.y);
		
		//limita aceleração pelo MaxSpeed
		if (rigidbodyPlayer.velocity.x > maxSpeed) {
			rigidbodyPlayer.velocity = new Vector2 (maxSpeed, rigidbodyPlayer.velocity.y);
		}
		if (rigidbodyPlayer.velocity.x < -maxSpeed) {
			rigidbodyPlayer.velocity = new Vector2 (-maxSpeed, rigidbodyPlayer.velocity.y);
		}

		//define valor da var velocidade do animador que executa animações //animação executa com base na velocidade >0 -- https://youtu.be/-iq_Yt6iNZc
		playerAnimator.SetFloat ("velocity", Mathf.Abs(inputDirection.x));

	}

	public void Jump(){

		//o player está no chão se a linecast encostar em qualquer coisa no layer dado
		//pega layer ground procura numero e compara se é essa q colediu, se for igual a 1 colidiu
		//bool isG recebe result V ou F
		isGrounded = Physics2D.Linecast (transform.position, footCollision.position, 1 << LayerMask.NameToLayer("Ground"));

		//Force emprega impulso no objeto rigibody, time.delta - deixa o fps padrão a todas as máquinas
		if (isGrounded) {
			rigidbodyPlayer.AddForce (new Vector2 (0, speedJump ));
			playerAnimator.SetTrigger("jump");
		}
	}

	public void Animations(){
		//atualiza a variavel da animação com o valor do Linecast
		playerAnimator.SetBool("isGrounded", isGrounded);

		//define velocidade de queda. fins de animação
		playerAnimator.SetFloat ("vSpeed", rigidbodyPlayer.velocity.y);

	}

	void OnTriggerEnter2D(Collider2D col){
		
		if (col.tag == "water" ) {
            Die();            
        }
	}

	public void Die()
	{
        
        if (!died)
        {
            died = true;
            GameMaster.KillPlayer(this);
        }
        
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
	//trata interação com inimigo
	EnemyBehaviour enemy = collision.collider.GetComponent<EnemyBehaviour>();

		if (enemy != null) {
		 
		foreach (ContactPoint2D point in collision.contacts) { // verifica pontos de contato do collider
				//Debug.Log (point.normal);
				//Debug.DrawLine (point.point, point.point + point.normal, Color.red, 10);
				if (point.normal.y >= 0.10f) {//anterior 09// Detecta quando pontos em Y encostam //deixa uma marge angular para ainda matar o inimigo
				Vector2 velocity = rigidbodyPlayer.velocity;
				velocity.y = speedJump;
				//rigidbodyPlayer.AddForce (new Vector2 (0, speedJump ));// gera bug quando pula do alto...
					rigidbodyPlayer.velocity = (velocity*Time.deltaTime); //aplica força do pulo
				enemy.Die ();
				}
                else {
                    Die();
                }
			}
		}
	}
}
