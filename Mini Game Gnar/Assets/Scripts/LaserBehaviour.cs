﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBehaviour : MonoBehaviour {

    private Transform player;
    public float speed;    
    private Rigidbody2D rb;    
    private bool searchingForPlayer = false;
    
    // Use this for initialization
    void OnEnable()
    {
        //player = GameObject.Find ("Player").transform;       
        speed *= Time.deltaTime;

        if (player == null) // inicia os IEnumerators/Corrotines que buscam o player.
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }
    }
    IEnumerator SearchForPlayer()
    {//busca se o player existe
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
            //player = enemy; //test
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            player = sResult.transform;
            searchingForPlayer = false;
            StartCoroutine(UpdatePath());

            yield return false;
        }
    }
    IEnumerator UpdatePath()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            yield return false;
        }       
    }
    
    void Update()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());//"para" a exec até terminar 
            }
            return;
        }
		//Debug.Log("SAINDO LASER PREFAB!");
	}

    public void Die()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" )
        {
            PlayerBehavior player = col.GetComponent<PlayerBehavior>();           
            player.Die(); //GetComponent<EnemyBehaviour> ()Die();          
            Destroy(gameObject);
            Debug.Log("Toma player!");
        }       

        if (col.tag == "ground")
        {
            Destroy(gameObject);
            Debug.Log("Laser destruido!");
        }
    }
}

