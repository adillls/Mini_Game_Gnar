﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangBehaviour : MonoBehaviour {

	public Animator boomerangAnimator;

	private Transform player;
	public float speed;
	public bool returning = false;
	public float timer;
	public float disableTimer;
	private Rigidbody2D rb;
	private bool goToRight=true;
	private Vector2 lastPosition;
	private bool disable = false;
	private bool execOnce =true;
    private bool searchingForPlayer = false;

    // Use this for initialization
    void OnEnable () {
		//player = GameObject.Find ("Player").transform;
		timer = 0.0f;
		speed *= Time.deltaTime;

        if (player == null) // inicia os IEnumerators/Corrotines que buscam o player.
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }
    }



    IEnumerator SearchForPlayer()
    {//
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
			yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            player = sResult.transform;
            searchingForPlayer = false;
            StartCoroutine(UpdatePath());

            yield return false;
        }
    }
    IEnumerator UpdatePath()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            yield return false;
        }
    }


    // Update is called once per frame
    void Update () {

        if (player == null)
        {
			Destroy(gameObject);
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());//"para" o resto e exec até terminar 
            }
            return;
        }

        timer += Time.deltaTime;


		if (timer >= 0.7f && !returning) {			
			lastPosition = player.position;
            returning = true;
            //Debug.Log("agora volte!");
        }
		if (!returning) {
			Vector3 correctDirection;
			if(goToRight){
				correctDirection = Vector3.right;
			}
			else{
				correctDirection = Vector3.left;
			}	
			transform.Translate (correctDirection * speed);//vai
			//Debug.Log (disable);

			//-----------------------------------

		} else if(returning && !disable) {			
			transform.position = Vector2.Lerp (transform.position, lastPosition, speed);//volta
			//Debug.Log (transform.position);
			disableTimer += Time.deltaTime;
		}
		if(disableTimer >= 2f){ //desativa dano do boomerang se ele ficar 2sg no ar
			disable = true; //Debug.Log ("disable do else = "+ disable); fazer animação do boome parado

			boomerangAnimator.SetBool ("disable", disable);

			RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down, 100,  1 << LayerMask.NameToLayer("Ground"));

			//Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition)*100, Color.cyan);
			if (hit.collider != null) {
				Debug.DrawLine (transform.position, hit.point, Color.red);
				transform.position = Vector2.Lerp (new Vector2 (transform.position.x, transform.position.y), hit.point , speed*2);
				Debug.Log("hit = "+ hit.point);

				//transform.Translate (hit.point);
			}

            //codigo que faz o boomerang cair no chao -- arrumar bug
                            //ativar a gravidade e desativar o is trigger do collider
                            //BoomerangPrefab.rigidbody2D.gravityScale = 1;
                            //Physics2D.gravity = new Vector2(1,0);
                            //BoomerangPrefab.GetComponent<BoomerangBehaviour> ().boomer.isKinematic=false;

            //BoomerangPrefab.GetComponent<BoomerangBehaviour>(). Rigidbody2D.isKinematic =true;
            //	Collider2D.isTrigger = false;

            //transform.position = Vector2.Lerp (lastPosition, transform.position.y = 200f, speed);
            //RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down,1 << LayerMask.NameToLayer("Ground"));
            /*
			Debug.DrawRay (transform.position, Vector2.down, Color.yellow);

			RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down, 1 << LayerMask.NameToLayer("Ground"));
			if (hit) {
				//transform.Translate (hit.transform.position);
				Debug.Log("hit = "+ hit.transform.position);
				Debug.Log("last = "+lastPosition);
				transform.position = Vector2.Lerp (lastPosition, hit.point, speed);
				//transform.position = hit.transform.position;
				//Debug.Log (hit.collider.name);
				Debug.Log (hit.collider.name);
			}
*/
            /*
			RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector2.down, 100,  1 << LayerMask.NameToLayer("Ground"));

			//Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition)*100, Color.cyan);
			if (hit.collider != null) {
				Debug.DrawLine (transform.position, hit.point, Color.red);
				transform.position = Vector2.Lerp (new Vector2 (transform.position.x, transform.position.y), hit.point , speed);
				Debug.Log("hit = "+ hit.point);

				//transform.Translate (hit.point);
			}
			*/
            /*
			RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, Vector2.down, 1 << LayerMask.NameToLayer("Ground"));

			//If something was hit.
			if (hit.collider != null)
			{
				 
				transform.position = Vector2.Lerp (new Vector2 (transform.position.x, transform.position.y), new Vector2  (transform.InverseTransformPoint (hit.point)) , speed);
				Debug.Log(hit.point);
			}
			*/
            /*
                    execOnce = false;
                    RaycastHit2D hit = Physics2D.Raycast (transform.position, -transform.up, 1 << LayerMask.NameToLayer ("Ground"));

                    //Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition)*100, Color.cyan);
                    if (hit.collider != null) {
                        Debug.DrawLine (transform.position, hit.point, Color.red);
                        //transform.position = Vector2.Lerp (transform.position, new Vector2 (transform.position.x, (transform.position.y - transform.localScale.y) - hit.distance), speed);
                        //transform.Translate (new Vector2(lastPosition.x , (transform.position.y - transform.localScale.y) - hit.distance));
                        transform.position = hit.transform.position;
                        //transform.Translate (correctDirection * speed);//vai


                        Debug.Log ("hit = " + (transform.position.y - hit.distance));
                    }


                RaycastHit hit;
                Physics.Raycast(new Ray(transform.position, Vector3.down), out hit);
                Vector3 distance = (transform.position.x , (transform.position.y - hit.point.y) *-1 , 0);
                //you need to determine height of your character yourself
                //I assume that character center is in the middle
                if (hit.collider != null)
                {
                    Debug.DrawLine(transform.position, hit.point, Color.red);
                    transform.position = Vector3.Lerp (transform.position,distance, speed);
                    //transform.Translate (new Vector2(lastPosition.x , (transform.position.y - transform.localScale.y) - hit.distance));               
                    //transform.Translate (correctDirection * speed);//vai


                    Debug.Log("hit = " + (transform.position.y - hit.distance));
                }*/
			}
    }

	public void setDirection(bool right)	{
		goToRight = right;
		if(!right){
			Vector3 newDirection = transform.localScale;
			newDirection.x *= -1;
			transform.localScale = newDirection;
		}
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && returning==true ) {			
			//Debug.Log ("Aki no boome 1ta: " + returned);
			Destroy(gameObject);
		}

		if (col.tag == "zzrot") {

			//Debug.Log ("Acertou inimigo "+ disable);
			EnemyBehaviour enemy = col.GetComponent<EnemyBehaviour> ();
			if (disable == false) {
				enemy.Die (); //GetComponent<EnemyBehaviour> ()Die();
				Debug.Log ("Acertou inimigo" + disable);
			}

			timer = 0.7f;
		}

        if (col.tag == "laser")
        {

            //Debug.Log ("Acertou inimigo "+ disable);
            LaserBehaviour laser = col.GetComponent<LaserBehaviour>();
            if (disable == false)
            {
                laser.Die();
                
            }

            timer = 0.7f;
        }
    }
}

