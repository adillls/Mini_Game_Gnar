# Mini game Gnar - projeto de PRJ - Oficina Mentoria


O principal público qual o jogo é destinado, é para toda comunidade de League of Legends, um dos mais populares jogos eletrônicos, qual esse fan game se baseia, logo em sua maioria a jovens e adolescentes de todo mundo.

Todos que se dispõem a jogar buscam novas experiências, aos amantes do título indie esse fan game visa trazer a diversão e desafio qual buscam, em um mini game com simplicidade e carisma.

O objetivo desse fan game é trazer como personagem, o pré-histórico Gnar, com toda sua mecânica dos campos de batalha (League of Legends) adaptadas para dentro de um mini game 2D, pixel art, e claro, contar também um pouco mais de suas misteriosa história esquecida e aqui adaptada.
